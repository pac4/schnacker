import json

idsFile = "/home/carsten/schnackIds.txt"
inputFile = "the-tale-of-robby.schnack"

ids = []

with open(idsFile, "r") as f:
    ids = list(map(lambda x: x.strip(), f.readlines()))

def mapId(id):
    id = int(id)
    return ids[id]

with open(inputFile, "r+") as f:
    contents = json.loads(f.read())

    contents["schnackVersion"] = "0.4.0"

    # localization
    newLocalization = {}
    for key, value in contents["localization"].items():
        newLocalization[mapId(key)] = value

    contents["localization"] = newLocalization

    # dialogs
    for dialog in contents["dialogs"]:
        dialog["entryNode"] = mapId(dialog["entryNode"])
        for node in dialog["nodes"]:
            node["id"] = mapId(node["id"])
            node["next"] = list(map(lambda x: mapId(x), node["next"]))

    f.seek(0)
    f.write(json.dumps(contents, indent = 4))