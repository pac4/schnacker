#include <iostream>
#include <fstream>
#include "schnacker.hpp"

using namespace schnacker;

int main(int argc, char *argv[])
{
    std::shared_ptr<SchnackFile> schnackFile;

    if(argc == 1)
    {
        std::cout << "no file path provided, creating dummy dialog...\n";

        schnackFile = std::make_shared<SchnackFile>();
        schnackFile->locales.push_back("en");
        schnackFile->locales.push_back("de");
        schnackFile->localization["0000000000"] = std::vector<std::string>
        {
            "Hello, my name is M.I.L.E.S.!\nPlease state the nature of the medical emergency!",
            "Hallo, mein Name ist M.I.L.E.S.!\nBitte nennen Sie die Art des medizinischen Notfalls!"
        };
        schnackFile->localization["1111111111"] = std::vector<std::string>
        {
            "Everything is destroyed. I can't find the other crew members.",
            "Hier ist alles zerstört. Ich kann die anderen Crewmitglieder nicht finden."
        };
        schnackFile->localization["2222222222"] = std::vector<std::string>
        {
            "Everything is allright, there is no emergency.",
            "Alles in Ordnung, es gibt keinen Notfall."
        };
        schnackFile->localization["3333333333"] = std::vector<std::string>
        {
            "AAaaah heeeelp! Oh my goto... panic!!!!!",
            "AAaaah Hilfeee! Oh mein Goto...Panik!!!!!"
        };
        schnackFile->localization["4444444444"] = std::vector<std::string>
        {
            "Oh - I see, no emergency...",
            "Achso, doch kein Notfall?"
        };

        // -- build a dialog --
        auto milesDialog = std::make_shared<Dialog>();
        milesDialog->name = "miles";
        milesDialog->entryNode = "0000000000";
        milesDialog->schnackFile = schnackFile;
        schnackFile->dialogs[milesDialog->name] = milesDialog;

        auto milesChar = std::make_shared<Character>();
        milesChar->canonicalName = "miles";
        milesChar->displayName = "M.I.L.E.S.";
        schnackFile->characters[milesChar->canonicalName] = milesChar;

        auto textNode = std::make_shared<TextNode>();
        textNode->id = "0000000000";
        textNode->character = milesChar;
        textNode->next.push_back("bbbbbbbbbb");
        milesDialog->nodes["0000000000"] = textNode;

        auto questionNode = std::make_shared<QuestionNode>();
        questionNode->id = "bbbbbbbbbb",
        questionNode->next.push_back("1111111111");
        questionNode->next.push_back("2222222222");
        milesDialog->nodes["bbbbbbbbbb"] = questionNode;

        auto answerNode = std::make_shared<AnswerNode>();
        answerNode->id = "1111111111",
        answerNode->next.push_back("3333333333");
        milesDialog->nodes["1111111111"] = answerNode;

        answerNode = std::make_shared<AnswerNode>();
        answerNode->id = "2222222222",
        answerNode->next.push_back("4444444444");
        milesDialog->nodes["2222222222"] = answerNode;

        textNode = std::make_shared<TextNode>();
        textNode->id = "3333333333";
        textNode->character = milesChar;
        milesDialog->nodes["3333333333"] = textNode;

        textNode = std::make_shared<TextNode>();
        textNode->id = "4444444444";
        textNode->character = milesChar;
        milesDialog->nodes["4444444444"] = textNode;

        std::cout << "...creating dummy dialog is done.\n\n";
    }
    else if(argc > 1)
    {
        std::ifstream stream(argv[1]);
        std::stringstream buffer;
        buffer << stream.rdbuf();

        schnackFile = SchnackFile::loadFromString(buffer.str());
    }

    while(true)
    {
        std::cout << std::endl << std::endl << "Please select a dialog:" << std::endl;
        std::cout << "-----------------------" << std::endl;
        
        int i = 1;
        for(auto const& dialog : schnackFile->dialogs) {
            std::cout << "  " << i << ") " << dialog.first << std::endl;
            i++;
        }
        std::cout << "  " << i << ") <exit>" << std::endl;
        
        std::string dialogSelection;
        std::getline(std::cin, dialogSelection);
        int dialogSelectionIndex = std::atoi(dialogSelection.c_str());

        if(dialogSelectionIndex == i)
            break;
        if(dialogSelectionIndex <= 0 || dialogSelectionIndex > schnackFile->dialogs.size())
            continue;

        dialogSelectionIndex--; // zero-based index

        auto dialogsIterator = schnackFile->dialogs.begin();
        for(int dlgIdx = 0; dlgIdx < dialogSelectionIndex; dlgIdx++)
            dialogsIterator++;

        auto dialogName = dialogsIterator->first;
        auto dialog = dialogsIterator->second;

        // -- play the dialog --
        std::cout << std::endl << "Playing dialog: " << dialogName << std::endl;
        std::cout << "--------------- " << std::endl;
        auto currentNode = dialog->getEntryNode();
        if(!currentNode->checkPrecondition(dialog))
            continue;

        while(true)
        {
            if(currentNode == nullptr)
                break;

            auto result = currentNode->step(dialog);
            if(result == nullptr) {
                std::cout << "result is null\n";
                return 0;
            }

            currentNode = result->currentNode;
            dialog = result->currentDialog;

            auto textResult = std::dynamic_pointer_cast<TextStepResult>(result);
            if(textResult)
            {
                std::cout << textResult->character->displayName << ": " << textResult->text << "\n\n";
            }
            else
            {
                auto answersResult = std::dynamic_pointer_cast<AnswersStepResult>(result);
                if(answersResult)
                {
                    auto answerCount = answersResult->answers.size();
                    for(size_t i = 0; i < answerCount; i++)
                    {
                        NodeId id;
                        std::string text;
                        std::tie(id, text) = answersResult->answers[i];

                        char c = 97 + i;
                        std::cout << c << ") " << text << "\n";
                    }
                    std::cout << "\n";

                    std::string input;
                    while(true)
                    {
                        std::getline(std::cin, input);
                        auto choice = input;
                        auto choiceIndex = ((int)input[0]) - 97;
                        if(choiceIndex < 0 || choiceIndex >= answersResult->answers.size())
                            continue;

                        currentNode = answersResult->chooseAnswer(dialog, std::get<0>(answersResult->answers[choiceIndex]));
                        break;
                    }
                }
            }
        }
    }

    return 0;
}
