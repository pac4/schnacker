#pragma once

#include <string>
#include <vector>

#include "nodes.hpp"
#include "character.hpp"

namespace schnacker
{
    class StepResult
    {
    public:
        StepResult() = default;
        virtual ~StepResult() = default;
        std::shared_ptr<Node> currentNode;
        std::shared_ptr<Dialog> currentDialog;
        virtual void _();
    };

    class TextStepResult : public StepResult
    {
    public:
        std::string text;
        NodeId nodeId;
        std::shared_ptr<Character> character;
    };

    class AnswersStepResult : public StepResult
    {
    public:
        std::vector<std::tuple<NodeId, std::string>> answers;
        std::shared_ptr<Node> chooseAnswer(std::shared_ptr<Dialog> dialog, NodeId nodeId);
    };
}
