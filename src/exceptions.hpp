#pragma once

#include <stdexcept>
#include "stepresults.hpp"

namespace schnacker
{
    class SchnackException : public std::runtime_error
    {
    public:
        explicit SchnackException(const char *details);
    };
}
