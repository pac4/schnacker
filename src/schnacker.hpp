#pragma once

#include <map>
#include <string>
#include <vector>
#include <cctype>
#include <algorithm>

#include "luavalue.hpp"
#include "dialog.hpp"
#include "stepresults.hpp"
#include "exceptions.hpp"

#include <yaml-cpp/yaml.h>
#include <sol/sol.hpp>


namespace schnacker
{
    class SchnackFile
    {
    public:
        std::string name;
        std::map<std::string, LuaValue> variables;
        std::map<std::string, std::shared_ptr<Dialog>> dialogs;
        std::map<std::string, std::shared_ptr<Character>> characters;
        static std::shared_ptr<SchnackFile> loadFromString(std::string contents);
        static std::shared_ptr<SchnackFile> loadFromString(std::string contents, bool initializeVariables);
        static std::shared_ptr<SchnackFile> loadFromString(std::shared_ptr<sol::state> luaState, std::string contents, bool initializeVariables);
        static std::shared_ptr<sol::state> initializeLua();
        std::shared_ptr<sol::state> luaState;
        std::vector<std::string> locales;
        std::map<NodeId, std::vector<std::string>> localization;
        std::string getCurrentLocale();
        void setCurrentLocale(std::string locale);
        std::string localize(NodeId nodeId);
    private:
        void initializeVariables();
        int _currentLocaleIndex = 0;

        static std::string StringToLower(std::string input)
        {
            std::transform(input.begin(), input.end(), input.begin(), [](unsigned char c){ return std::tolower(c); });
            return input;
        };

        static void decodeVariables(std::map<std::string, LuaValue> &variables, const YAML::Node& yamlNode)
        {
            if(!yamlNode.IsMap())
                return;

            for (const auto &property : yamlNode)
            {
                LuaValue value;
                auto name = property.first.as<std::string>();
                auto jsonValue = property.second;
                bool valueSet = false;

                if(jsonValue.IsNull())
                {
                    value.type = LuaValueType::Nil;
                    valueSet = true;
                }
                else if(!jsonValue.IsScalar())
                {
                    throw new SchnackException("variable values must be number, string, bool or null");
                }
                else
                {
                    auto strValue = jsonValue.as<std::string>();

                    if(jsonValue.Tag() == "?") // not quoted (i.e. possibly not a string)
                    {
                        if((StringToLower(strValue) == "true"))
                        {
                            value.type = LuaValueType::Boolean;
                            value.boolValue = true;
                            valueSet = true;
                        }
                        else if((StringToLower(strValue) == "false"))
                        {
                            value.type = LuaValueType::Boolean;
                            value.boolValue = false;
                            valueSet = true;
                        }
                        else
                        {
                            std::size_t pos;
                            try
                            {
                                double numberValue = std::stod(strValue, &pos);
                                if(pos == strValue.size())
                                {
                                    value.type = LuaValueType::Number;
                                    value.numberValue = numberValue;
                                    valueSet = true;
                                }
                            }
                            catch(...)
                            {
                                // if it's not parseable, we guess it's a string
                            }
                        }
                    }

                    if(!valueSet)
                    {
                        value.type = LuaValueType::String;
                        value.stringValue = strValue;
                    }
                }
                variables[name] = value;
            }
        };

        static std::shared_ptr<Character> decodeCharacter(const YAML::Node& yamlNode)
        {
            if(!yamlNode.IsMap())
                return nullptr;

            auto character = std::make_shared<Character>();

            if(yamlNode["canonicalName"].IsDefined())
                character->canonicalName = yamlNode["canonicalName"].as<std::string>();
            else
                character->canonicalName = "";

            if(yamlNode["displayName"].IsDefined())
                character->displayName = yamlNode["displayName"].as<std::string>();
            else
                character->displayName = "";

            if(yamlNode["color"].IsDefined())
                character->color = yamlNode["color"].as<std::string>();
            else
                character->color = "";

            if(yamlNode["properties"].IsDefined())
                decodeVariables(character->properties, yamlNode["properties"]);

            return character;
        };

        static std::shared_ptr<Node> decodeNode(const YAML::Node& yamlNode,
                                                std::map<std::string, std::shared_ptr<Character>> &characters,
                                                std::map<std::string, std::shared_ptr<Dialog>> &dialogs)
        {
            if(!yamlNode.IsMap())
                return nullptr;

            std::shared_ptr<Node> schnackNode;
            std::string type;

            if(yamlNode["type"].IsDefined())
                type = yamlNode["type"].as<std::string>();
            else
                throw new SchnackException("Node must declare a type");

            if(type == "answer")
            {
                auto answerNode = std::make_shared<AnswerNode>();
                schnackNode = answerNode;
            }
            else if(type == "branch")
            {
                auto branchNode = std::make_shared<BranchNode>();
                schnackNode = branchNode;
            }
            else if(type == "empty")
            {
                auto emptyNode = std::make_shared<EmptyNode>();
                schnackNode = emptyNode;
            }
            else if(type == "note")
            {
                auto noteNode = std::make_shared<NoteNode>();
                schnackNode = noteNode;

                if(yamlNode["text"].IsDefined())
                    noteNode->text = yamlNode["text"].as<std::string>();
                else
                    noteNode->text = "";
            }
            else if(type == "question")
            {
                auto questionNode = std::make_shared<QuestionNode>();
                schnackNode = questionNode;
            }
            else if(type == "random")
            {
                auto randomNode = std::make_shared<RandomNode>();
                schnackNode = randomNode;
            }
            else if(type == "cycle")
            {
                auto cycleNode = std::make_shared<CycleNode>();
                schnackNode = cycleNode;
            }
            else if(type == "reference")
            {
                auto referenceNode = std::make_shared<ReferenceNode>();
                schnackNode = referenceNode;
                auto refDialogName = yamlNode["dialog"].as<std::string>();

                if(dialogs.count(refDialogName) == 0)
                    throw new SchnackException("referenced dialog of ReferenceNode could not be resolved");

                referenceNode->referencedDialog = dialogs[refDialogName];
            }
            else if(type == "text")
            {
                auto textNode = std::make_shared<TextNode>();
                schnackNode = textNode;
                auto characterCanonicalName = yamlNode["character"].as<std::string>();

                if(characters.count(characterCanonicalName) == 0)
                    throw new SchnackException("Character of TextNode could not be resolved");

                textNode->character = characters[characterCanonicalName];
            }
            else
            {
                throw new SchnackException("Unknown node type");
            }

            schnackNode->id = yamlNode["id"].as<NodeId>();

            if(yamlNode["pos"].IsDefined())
            {
                schnackNode->pos.x = yamlNode["pos"]["x"].as<double>();
                schnackNode->pos.y = yamlNode["pos"]["y"].as<double>();
            }

            if(yamlNode["onlyOnce"].IsDefined())
                schnackNode->onlyOnce = yamlNode["onlyOnce"].as<bool>();
            else
                schnackNode->onlyOnce = false;


            if(yamlNode["next"].IsDefined())
            {
                auto nextList = yamlNode["next"];
                if(nextList.IsSequence())
                {
                    for(unsigned int i = 0; i < nextList.size(); i++)
                    {
                        schnackNode->next.push_back(nextList[i].as<std::string>());
                    }
                }
            }

            if(yamlNode["precondition"].IsDefined())
                schnackNode->precondition = yamlNode["precondition"].as<std::string>();
            else
                schnackNode->precondition = "";

            if(yamlNode["script"].IsDefined())
                schnackNode->script = yamlNode["script"].as<std::string>();
            else
                schnackNode->script = "";

            return schnackNode;
        };
    };
}
