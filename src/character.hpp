#pragma once

#include <string>
#include "luavalue.hpp"

namespace schnacker
{
    class Character
    {
    public:
        std::string displayName;
        std::string canonicalName;
        std::string color;
        std::map<std::string, LuaValue> properties;
    };
}

