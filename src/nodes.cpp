#include "nodes.hpp"
#include "dialog.hpp"
#include "exceptions.hpp"
#include "schnacker.hpp"

using namespace schnacker;

std::shared_ptr<StepResult> Node::step(std::shared_ptr<Dialog> dialog)
{
    if(!script.empty())
        dialog->schnackFile.lock()->luaState->script(script);

    if(onlyOnce)
        (*dialog->schnackFile.lock()->luaState)[ONLY_ONCE_TABLE][id] = true;

    return nullptr;
}

std::shared_ptr<Node> Node::getFirstPossibleSuccessor(std::shared_ptr<Dialog> dialog)
{
    return getFirstPossibleSuccessor(dialog, 0);
}

std::shared_ptr<Node> Node::getFirstPossibleSuccessor(std::shared_ptr<Dialog> dialog, int startIndex)
{
    if (!dialog)
        return nullptr;
    if (next.size() < 1)
        return nullptr;
    if (dialog->nodes.count(next[0]) < 1)
        return nullptr;

    for(size_t i = startIndex; i < next.size(); i++)
    {
        if(dialog->nodes[next[i]]->checkPrecondition(dialog))
        {
            return dialog->nodes[next[i]];
        }
    }
    return nullptr;
}

bool Node::checkPrecondition(std::shared_ptr<Dialog> dialog)
{
    return (!onlyOnce || (*dialog->schnackFile.lock()->luaState)[ONLY_ONCE_TABLE][id] != true)
        &&(precondition.empty() || dialog->schnackFile.lock()->luaState->script(precondition));
}

std::vector<std::shared_ptr<Node>> Node::getPossibleSuccessors(std::shared_ptr<Dialog> dialog)
{
    std::vector<std::shared_ptr<Node>> result;

    if (!dialog)
        return result;
    if (next.size() < 1)
        return result;
    if (dialog->nodes.count(next[0]) < 1)
        return result;

    auto successorCount = next.size();
    for(size_t i = 0; i < successorCount; i++)
    {
        if(dialog->nodes[next[i]]->checkPrecondition(dialog))
            result.push_back(dialog->nodes[next[i]]);
    }

    return result;
}

std::shared_ptr<StepResult> BranchNode::step(std::shared_ptr<Dialog> dialog)
{
    auto successor = getFirstPossibleSuccessor(dialog);
    Node::step(dialog);
    return successor != nullptr ? successor->step(dialog) : nullptr;
}

std::shared_ptr<StepResult> RandomNode::step(std::shared_ptr<Dialog> dialog)
{
    auto possibleSuccessors = getPossibleSuccessors(dialog);

    if (possibleSuccessors.empty())
        return nullptr;

    auto randomNumber = std::rand() % possibleSuccessors.size();
    auto successor = possibleSuccessors[randomNumber];
    Node::step(dialog);
    return successor != nullptr ? successor->step(dialog) : nullptr;
}

std::shared_ptr<StepResult> CycleNode::step(std::shared_ptr<Dialog> dialog)
{
    if((*dialog->schnackFile.lock()->luaState)[CYCLE_TABLE][id]  == sol::lua_nil)
       (*dialog->schnackFile.lock()->luaState)[CYCLE_TABLE][id] = 0;

    int startIndex = (*dialog->schnackFile.lock()->luaState)[CYCLE_TABLE][id];

    auto successor = getFirstPossibleSuccessor(dialog, startIndex);
    Node::step(dialog);

    startIndex++;
    startIndex %= next.size();
    (*dialog->schnackFile.lock()->luaState)[CYCLE_TABLE][id] = startIndex;

    return successor != nullptr ? successor->step(dialog) : nullptr;
}

std::shared_ptr<StepResult> ReferenceNode::step(std::shared_ptr<Dialog> dialog)
{
    auto successor = referencedDialog->getEntryNode();
    Node::step(dialog);
    return successor != nullptr ? successor->step(referencedDialog) : nullptr;
}

std::shared_ptr<StepResult> EmptyNode::step(std::shared_ptr<Dialog> dialog)
{
    auto successor = getFirstPossibleSuccessor(dialog);
    Node::step(dialog);
    return successor != nullptr ? successor->step(dialog) : nullptr;
}

std::shared_ptr<StepResult> NoteNode::step(std::shared_ptr<Dialog> dialog)
{
    throw new SchnackException((std::string("Unable to execute comment node (notes are for documentation purposes only) Node ID: ") + id).c_str());
}

std::shared_ptr<StepResult> QuestionNode::step(std::shared_ptr<Dialog> dialog)
{
    auto possibleAnswers = getPossibleSuccessors(dialog);

    if (possibleAnswers.empty())
        return nullptr;

    auto result = std::make_shared<AnswersStepResult>();
    result->currentDialog = dialog;
    auto answerCount = possibleAnswers.size();
    for (size_t i = 0; i < answerCount; i++)
    {
        auto answer = std::dynamic_pointer_cast<AnswerNode>(possibleAnswers[i]);
        result->answers.push_back(std::tuple<NodeId, std::string>(answer->id, dialog->schnackFile.lock()->localize(answer->id)));
    }
    Node::step(dialog);
    return result;
}

std::shared_ptr<StepResult> AnswerNode::step(std::shared_ptr<Dialog> dialog)
{
    auto successor = getFirstPossibleSuccessor(dialog);
    Node::step(dialog);
    return successor != nullptr ? successor->step(dialog) : nullptr;
}

std::shared_ptr<StepResult> TextNode::step(std::shared_ptr<Dialog> dialog)
{
    auto successor = getFirstPossibleSuccessor(dialog);
    auto result = std::make_shared<TextStepResult>();
    result->currentDialog = dialog;
    result->text = dialog->schnackFile.lock()->localize(id);
    result->nodeId = id;
    result->character = character;
    result->currentNode = successor;
    Node::step(dialog);
    return result;
}
