#pragma once

#include <string>
#include <vector>
#include <memory>

#define ONLY_ONCE_TABLE "_schnackOnlyOnceTable"
#define CYCLE_TABLE "_schnackCycleTable"

namespace schnacker
{
    class Dialog;
    class Character;
    class StepResult;

    typedef std::string NodeId;

    class Position
    {
    public:
        double x = 0;
        double y = 0;
    };

    class Node
    {
    public:
        Node() = default;
        virtual ~Node() = default;
        NodeId id;
        Position pos;
        bool onlyOnce;
        std::vector<NodeId> next;
        std::string precondition;
        std::string script;
        virtual std::shared_ptr<StepResult> step(std::shared_ptr<Dialog> dialog);

        std::shared_ptr<Node> getFirstPossibleSuccessor(std::shared_ptr<Dialog> dialog);
        std::shared_ptr<Node> getFirstPossibleSuccessor(std::shared_ptr<Dialog> dialog, int startIndex);
        std::vector<std::shared_ptr<Node>> getPossibleSuccessors(std::shared_ptr<Dialog> dialog);
        bool checkPrecondition(std::shared_ptr<Dialog> dialog);
    };

    class AnswerNode : public Node
    {
    public:
        std::shared_ptr<StepResult> step(std::shared_ptr<Dialog> dialog) override;
    };

    class BranchNode : public Node
    {
    public:
        std::shared_ptr<StepResult> step(std::shared_ptr<Dialog> dialog);
    };

    class EmptyNode : public Node
    {
    public:
        std::shared_ptr<StepResult> step(std::shared_ptr<Dialog> dialog);
    };

    class NoteNode : public Node
    {
    public:
        std::string text;
        std::shared_ptr<StepResult> step(std::shared_ptr<Dialog> dialog);
    };

    class QuestionNode : public Node
    {
    public:
        std::shared_ptr<StepResult> step(std::shared_ptr<Dialog> dialog);
    };

    class RandomNode : public Node
    {
    public:
        std::shared_ptr<StepResult> step(std::shared_ptr<Dialog> dialog);
    };

    class CycleNode : public Node
    {
    public:
        std::shared_ptr<StepResult> step(std::shared_ptr<Dialog> dialog);
    };

    class ReferenceNode : public Node
    {
    public:
        std::shared_ptr<Dialog> referencedDialog;
        std::shared_ptr<StepResult> step(std::shared_ptr<Dialog> dialog);
    };

    class TextNode : public Node
    {
    public:
        std::shared_ptr<Character> character;
        std::shared_ptr<StepResult> step(std::shared_ptr<Dialog> dialog);
    };
}
