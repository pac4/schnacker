#pragma once

#include <string>

namespace schnacker
{
    enum LuaValueType
    {
        Number,
        Boolean,
        String,
        Nil
    };

    struct LuaValue
    {
        LuaValueType type;
        union
        {
            double numberValue;
            bool boolValue;
        };
        std::string stringValue;
    };
}
