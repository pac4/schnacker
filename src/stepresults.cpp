#include "schnacker.hpp"

using namespace schnacker;


std::shared_ptr<Node> AnswersStepResult::chooseAnswer(std::shared_ptr<Dialog> dialog, NodeId nodeId)
{
    if(!dialog->nodes.count(nodeId))
        throw new SchnackException("The given node ID was not found!");


    auto successor = dialog->nodes[nodeId];
    if(!successor->checkPrecondition(dialog))
        return nullptr;

    return successor;
}

void StepResult::_()
{
}
