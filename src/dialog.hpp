#pragma once

#include <map>
#include <string>
#include <vector>
#include <memory>

#include "nodes.hpp"

namespace schnacker
{
    class SchnackFile;

    class Dialog
    {
    public:
        std::string name;
        NodeId entryNode;
        std::map<NodeId, std::shared_ptr<Node>> nodes;
        std::shared_ptr<Node> getEntryNode();
        std::weak_ptr<SchnackFile> schnackFile;
    };
}
