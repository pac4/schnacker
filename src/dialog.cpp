#include "dialog.hpp"

using namespace schnacker;

std::shared_ptr<Node> Dialog::getEntryNode()
{
    if (!entryNode.empty())
        return nodes[entryNode];

    return nullptr;
}
