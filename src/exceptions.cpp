#include "schnacker.hpp"

using namespace schnacker;

SchnackException::SchnackException(const char *details)
    : std::runtime_error(details)
{
}
