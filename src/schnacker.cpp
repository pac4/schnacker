#include <ctime>
#include "schnacker.hpp"

using namespace schnacker;

std::shared_ptr<sol::state> SchnackFile::initializeLua()
{
    auto luaState = std::make_shared<sol::state>();
	// open some common libraries
	luaState->open_libraries(sol::lib::base, sol::lib::package);
    return luaState;
}

std::shared_ptr<SchnackFile> SchnackFile::loadFromString(std::string contents)
{
    return loadFromString(contents, true);
}

std::shared_ptr<SchnackFile> SchnackFile::loadFromString(std::string contents, bool initializeVariables)
{
    return loadFromString(initializeLua(), contents, initializeVariables);
}

std::shared_ptr<SchnackFile> SchnackFile::loadFromString(std::shared_ptr<sol::state> luaState, std::string contents, bool initializeVariables)
{
    std::srand(std::time(0));

    YAML::Node json = YAML::Load(contents);
    auto schnackFile = std::make_shared<SchnackFile>();
    schnackFile->luaState = luaState;

    if(json["name"].IsDefined())
        schnackFile->name = json["name"].as<std::string>();

    if(json["characters"].IsDefined())
    {
        for (const auto &element : json["characters"])
        {
            auto character = decodeCharacter(element);
            schnackFile->characters[character->canonicalName] = character;
        }
    }

    if(json["locales"].IsDefined())
    {
        for (const auto &element : json["locales"])
        {
            auto locale = element.as<std::string>();
            schnackFile->locales.push_back(locale);
        }
    }

    if(json["localization"].IsDefined())
    {
        for (const auto &element : json["localization"])
        {
            auto key = element.first.as<NodeId>();
            auto values = element.second;
            std::vector<std::string> translations;
            for (const auto &value : values)
            {
                auto translation = value.as<std::string>();
                translations.push_back(translation);
            }
            schnackFile->localization[key] = translations;
        }
    }

    if(json["variables"].IsDefined())
        decodeVariables(schnackFile->variables, json["variables"]);

    // load dialogs first (without loading actual nodes)
    auto dialogs = json["dialogs"];
    for (const auto &jsonDialog : dialogs)
    {
        auto dialog = std::make_shared<Dialog>();
        dialog->name = jsonDialog["name"].as<std::string>();
        dialog->entryNode = jsonDialog["entryNode"].as<NodeId>();

        dialog->schnackFile = schnackFile;
        schnackFile->dialogs[dialog->name] = dialog;
    }
    // then load the nodes for each dialog
    // (since some nodes may reference dialogs by name and require their existence)
    for (const auto &jsonDialog : dialogs)
    {
        auto name = jsonDialog["name"].as<std::string>();
        auto dialog = schnackFile->dialogs[name];

        auto jsonNodes = jsonDialog["nodes"];
        for (const auto &element : jsonNodes)
        {
            auto node = decodeNode(element, schnackFile->characters, schnackFile->dialogs);
            dialog->nodes[node->id] = node;
        }
    }

    if(initializeVariables)
        schnackFile->initializeVariables();

    // init character table if it was not initialized properly
    if((*(luaState.get()))["characters"] == sol::lua_nil) {
        auto characterTable = luaState->create_table();

        for (const auto &character : schnackFile->characters)
        {
            auto charProperties = luaState->create_table();
            characterTable[character.second->canonicalName.c_str()] = charProperties;
        }
        (*(luaState.get()))["characters"] = characterTable;
    }

    // initialize onlyOnce table
    if((*(luaState.get()))[ONLY_ONCE_TABLE] == sol::lua_nil) {
        auto schnackOnlyOnceTable = luaState->create_table();
        (*(luaState.get()))[ONLY_ONCE_TABLE] = schnackOnlyOnceTable;
    }

    // initialize cycle node table
    if((*(luaState.get()))[CYCLE_TABLE] == sol::lua_nil) {
        auto schnackCycleTable = luaState->create_table();
        (*(luaState.get()))[CYCLE_TABLE] = schnackCycleTable;
    }

    return schnackFile;
}

std::string SchnackFile::getCurrentLocale()
{
    return locales[_currentLocaleIndex];
}

void SchnackFile::setCurrentLocale(std::string locale)
{
    for(int i = 0; i < locales.size(); i++)
    {
        if(locales[i] == locale)
        {
            _currentLocaleIndex = i;
            return;
        }
    }
}

std::string SchnackFile::localize(NodeId nodeId)
{
    if(localization.count(nodeId))
        if(localization[nodeId].size() > _currentLocaleIndex)
            return localization[nodeId][_currentLocaleIndex];
    return "";
}

std::string backupLuaTable(const sol::table table, const std::string &parent)
{
	std::string result = "";
	for (const auto &key_value_pair : table)
	{
		sol::object key = key_value_pair.first;
		sol::object value = key_value_pair.second;

		std::string k = "";
		if (key.get_type() == sol::type::string)
		{
			k = key.as<std::string>();
		}
		else if (key.get_type() == sol::type::number)
		{
			k = std::to_string(key.as<int>());
		}

		std::string v;

		if (k != "_entry_node" && k != "_VERSION" && k.substr(0, 4) != "sol." && k != "_G" && k != "base" && k != "package" && k != "searches")
		{

			switch (value.get_type())
			{
				// All this types are not saved in our savegame file.
			case sol::type::none:
			case sol::type::lua_nil:
			case sol::type::thread:
			case sol::type::function:
			case sol::type::userdata:
			case sol::type::poly:
			case sol::type::lightuserdata:
				break;

			case sol::type::string:
				v = value.as<std::string>();
				result += parent + k + " = \"" + v + "\"\n";
				break;
			case sol::type::number:
				v = std::to_string(value.as<double>());
				result += parent + k + " = " + v + "\n";
				break;
			case sol::type::boolean:

				if (value.as<bool>())
				{
					v = "true";
				}
				else
				{
					v = "false";
				}

				result += parent + k + " = " + v + "\n";
				break;
			case sol::type::table:
				result += parent + k + " = {}\n";
				result += backupLuaTable(value.as<sol::table>(), parent + k + ".");
				break;
			}
		}
	}

	return result;
}

void SchnackFile::initializeVariables()
{
    for (const auto &variable : variables)
    {
        switch(variable.second.type) {
            case LuaValueType::Number:
                (*(luaState.get()))[variable.first.c_str()] = variable.second.numberValue;
                break;
            case LuaValueType::Boolean:
                (*(luaState.get()))[variable.first.c_str()] = variable.second.boolValue;
                break;
            case LuaValueType::String:
                (*(luaState.get()))[variable.first.c_str()] = variable.second.stringValue;
                break;
            case LuaValueType::Nil:
                (*(luaState.get()))[variable.first.c_str()] = nullptr;
                break;
        }
    }

    auto characterTable = luaState->create_table();
    (*(luaState.get()))["characters"] = characterTable;

    for (const auto &character : characters)
    {
        auto charProperties = luaState->create_table();
        for(const auto &property : character.second->properties)
        {
            switch(property.second.type) {
                case LuaValueType::Number:
                    charProperties[property.first.c_str()] = property.second.numberValue;
                    break;
                case LuaValueType::Boolean:
                    charProperties[property.first.c_str()] = property.second.boolValue;
                    break;
                case LuaValueType::String:
                    charProperties[property.first.c_str()] = property.second.stringValue;
                    break;
                case LuaValueType::Nil:
                    charProperties[property.first.c_str()] = nullptr;
                    break;
            }
        }
        characterTable[character.second->canonicalName.c_str()] = charProperties;
    }
}
